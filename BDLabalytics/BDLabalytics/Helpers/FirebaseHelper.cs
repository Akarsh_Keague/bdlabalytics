﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDLabalytics.Models;
using Firebase.Database;
using Firebase.Database.Query;

namespace BDLabalytics.Helpers
{
    public class FirebaseHelper
    {
        private readonly string ChildName = "Users";

        readonly FirebaseClient firebase = new FirebaseClient("https://fir-23-cb28e-default-rtdb.firebaseio.com/");

        public async Task<UserModel> GetUser(string UID)
        {
            try
            {
                return (await firebase
                .Child(ChildName)
                .OnceAsync<UserModel>()).Select(item => new UserModel
                {
                    FirstName = item.Object.FirstName,
                    LastName = item.Object.LastName,
                    PhoneNo = item.Object.PhoneNo,
                    Email = item.Object.Email
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);

                return null;
            }
            
        }

        public async Task AddUser(UserModel userModel)
        {
            try
            {
                 await firebase
                .Child(ChildName)
                .PostAsync(new UserModel()
                {
                    UID = userModel.UID,
                    FirstName = userModel.FirstName,
                    LastName = userModel.LastName,
                    Email = userModel.Email,
                    PhoneNo = userModel.PhoneNo

                });
            }
            catch (Exception ex)
            {

            }
           
        }

        

        public async Task UpdatePerson(UserModel userModel)
        {
            var toUpdatePerson = (await firebase
                .Child(ChildName)
                .OnceAsync<UserModel>()).FirstOrDefault(a => a.Object.UID == userModel.UID);

            await firebase
                .Child(ChildName)
                .Child(toUpdatePerson.Key)
                .PutAsync(new UserModel()
                {
                    UID = userModel.UID,
                    FirstName = userModel.FirstName,
                    LastName = userModel.LastName,
                    Email = userModel.Email,
                    PhoneNo = userModel.PhoneNo

                });
        }

        
    }
}
