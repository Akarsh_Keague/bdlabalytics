﻿using System;
using System.Threading.Tasks;

namespace BDLabalytics.Helpers
{
    public class UserSettings
    {
        public static string UserName
        {
            get
            {
                string isLoggedIn = string.Empty;
                if (App.Current.Properties.ContainsKey("UserName"))
                {
                    isLoggedIn = Convert.ToString(App.Current.Properties["UserName"] ?? string.Empty);
                }
                return isLoggedIn;
            }
            set
            {
                if (App.Current.Properties.ContainsKey("UserName"))
                    App.Current.Properties.Remove("UserName");
                App.Current.Properties.Add("UserName", value);
                Task.Run(async () => await App.Current.SavePropertiesAsync());
            }
        }

        public static string UID
        {
            get
            {
                string isLoggedIn = string.Empty;
                if (App.Current.Properties.ContainsKey("UID"))
                {
                    isLoggedIn = Convert.ToString(App.Current.Properties["UID"] ?? string.Empty);
                }
                return isLoggedIn;
            }
            set
            {
                if (App.Current.Properties.ContainsKey("UID"))
                    App.Current.Properties.Remove("UID");
                App.Current.Properties.Add("UID", value);
                Task.Run(async () => await App.Current.SavePropertiesAsync());
            }
        }
    }
}
