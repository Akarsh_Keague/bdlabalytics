﻿using System;
namespace BDLabalytics.Models
{
    public class ProcessSpecimenModel: PatientModel
    {
        public string SpecimenID { get; set; }
        public string DeviceID { get; set; }
        public string ReportImage { get; set; }
        public string Result { get; set; }
        public string SendEmail { get; set; }

    }
}
