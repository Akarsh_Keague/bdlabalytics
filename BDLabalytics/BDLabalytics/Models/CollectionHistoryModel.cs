﻿using System;
namespace BDLabalytics.Models
{
    public class CollectionHistoryModel: PatientModel
    {
        public string UserID { get; set; }
        public string Status { get; set; }
        public string CollectionDateTime { get; set; }
        public string ProcessingDateTime { get; set; }
        public string TestType { get; set; }
        public string SpecimenID { get; set; }


    }
}
