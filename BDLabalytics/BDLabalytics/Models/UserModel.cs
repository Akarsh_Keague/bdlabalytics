﻿using System;
namespace BDLabalytics.Models
{
    public class UserModel
    {
        public string UID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
    }
    public class Users
    {
        public string Name { get; set; }
    }
}
