﻿using System;
namespace BDLabalytics.Models
{
    public class PatientDetailModel : PatientModel
    {
        public string Sex { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Race { get; set; }
        public string Ethnicity { get; set; }
    }
}
