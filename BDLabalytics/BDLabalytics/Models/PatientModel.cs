﻿using System;
namespace BDLabalytics.Models
{
    public class PatientModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Age { get; set; }
      
    }
}
