﻿using System;
namespace BDLabalytics.Models
{
    public class CollectionModel
    {
        public string TestingLocation { get; set; }
        public string TestType { get; set; }
        public string Address { get; set; }
        public string SpecimenID { get; set; }
        public string LotID { get; set; }
        public string ExpirationDate { get; set; }
    }
}
