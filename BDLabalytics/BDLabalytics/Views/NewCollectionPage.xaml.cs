﻿using System;
using System.Collections.Generic;
using BDLabalytics.Helpers;
using BDLabalytics.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDLabalytics.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewCollectionPage : ContentPage
    {
        NewCollectionViewModel newCollectionViewModel;
        public NewCollectionPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            newCollectionViewModel = ServiceContainer.Resolve<NewCollectionViewModel>();
            BindingContext = newCollectionViewModel;
        }
    }
}