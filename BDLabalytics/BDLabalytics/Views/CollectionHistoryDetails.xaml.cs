﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BDLabalytics.Views
{
    public partial class CollectionHistoryDetails : ContentPage
    {
        public CollectionHistoryDetails()
        {
            InitializeComponent();
        }
        private async void Back_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

    }
}
