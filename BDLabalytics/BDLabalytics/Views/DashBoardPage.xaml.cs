﻿using System;
using System.Collections.Generic;
using BDLabalytics.ViewModel;
using Xamarin.Forms;
using Autofac;
using BDLabalytics.Helpers;

namespace BDLabalytics.Views
{
    public partial class DashBoardPage : ContentPage
    {
        DashBoardViewModel _dashboardViewModel;
        public DashBoardPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            App.RegisterViewModels(Navigation);
            _dashboardViewModel = ServiceContainer.Resolve<DashBoardViewModel>();
            BindingContext = _dashboardViewModel;
        }
    }
}
