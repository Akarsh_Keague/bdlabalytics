﻿using System;
using System.Collections.Generic;
using BDLabalytics.Helpers;
using BDLabalytics.ViewModel;
using Xamarin.Forms;

namespace BDLabalytics.Views
{
    public partial class ProcessSpecimenCollectionComplete : ContentPage
    {
        ProcessSpecimenViewModel processSpecimenViewModel;
        public ProcessSpecimenCollectionComplete()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            processSpecimenViewModel = ServiceContainer.Resolve<ProcessSpecimenViewModel>();
            BindingContext = processSpecimenViewModel;
        }
    }
}
