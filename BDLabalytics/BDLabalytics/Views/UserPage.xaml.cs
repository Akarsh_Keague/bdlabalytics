﻿using System;
using System.Collections.Generic;
using Autofac;
using BDLabalytics.Helpers;
using BDLabalytics.Services;
using BDLabalytics.ViewModel;
using Xamarin.Forms;

namespace BDLabalytics.Views
{
    public partial class UserPage : ContentPage
    {
        UserViewModel userViewModel;
        public UserPage()
        {
            InitializeComponent();
            userViewModel = ServiceContainer.Resolve<UserViewModel>();
            BindingContext = userViewModel;
        }
        private async void Back_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            userViewModel.FetchUserDetail();
        }

    }
}
