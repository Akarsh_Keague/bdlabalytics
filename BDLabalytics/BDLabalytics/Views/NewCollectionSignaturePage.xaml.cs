﻿using System;
using System.Collections.Generic;
using BDLabalytics.Helpers;
using BDLabalytics.ViewModel;
using Xamarin.Forms;

namespace BDLabalytics.Views
{
    public partial class NewCollectionSignaturePage : ContentPage
    {
        NewCollectionViewModel newCollectionViewModel;
        public NewCollectionSignaturePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            newCollectionViewModel = ServiceContainer.Resolve<NewCollectionViewModel>();
            BindingContext = newCollectionViewModel;
        }
    }
}
