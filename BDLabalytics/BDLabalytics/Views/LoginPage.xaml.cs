﻿using System;
using System.Collections.Generic;
using Autofac;
using BDLabalytics.ViewModel;
using Xamarin.Forms;

namespace BDLabalytics.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

            this.BindingContext = (Application.Current as App).Container.Resolve<LoginViewModel>();
        }
    }
}
