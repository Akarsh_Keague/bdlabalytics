﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BDLabalytics.DataSource;
using BDLabalytics.ListViewAdapter;
using BDLabalytics.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDLabalytics.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CollectionHistoryPage : ContentPage
    {
        public CollectionHistoryPage()
        {
            InitializeComponent();
            var collectionList = CollectionHistoryDataSource.GetList();

            var groupedData =
                collectionList.OrderBy(e => e.FirstName)
                    .GroupBy(e => e.FirstName[0].ToString())
                    .Select(e => new ObservableGroupCollection<string, CollectionHistoryModel>(e))
                    .ToList();

            BindingContext = new ObservableCollection<ObservableGroupCollection<string, CollectionHistoryModel>>(groupedData);
        }

        private async void Back_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedItem = e.SelectedItem;

            if (e.SelectedItem == null) return;
            Navigation.PushModalAsync(new CollectionHistoryDetails());
        }

        
    }
}
