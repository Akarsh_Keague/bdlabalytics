﻿using System;
using System.Collections.Generic;
using BDLabalytics.Models;

namespace BDLabalytics.DataSource
{
    public class CollectionHistoryDataSource
    {
        public static List<CollectionHistoryModel> GetList()
        {
            var list = new List<CollectionHistoryModel>();

            list.Add(new CollectionHistoryModel() { FirstName = "Anderson", LastName = "Martin", DOB = DateTime.Now.Date.ToString(), Status ="Postive"});
            list.Add(new CollectionHistoryModel() { FirstName = "Adam", LastName = "Dev", DOB = DateTime.Now.Date.ToString(), Status = "Negative" });
            list.Add(new CollectionHistoryModel() { FirstName = "Bob", LastName = "Harry", DOB = DateTime.Now.Date.ToString(), Status = "Postive" });
            list.Add(new CollectionHistoryModel() { FirstName = "Brooke", LastName = "Clancy", DOB = DateTime.Now.Date.ToString(), Status = "Invalid" });
          
            return list;
        }
    }
}
