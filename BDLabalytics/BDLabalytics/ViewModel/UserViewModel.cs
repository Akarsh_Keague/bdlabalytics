﻿using System;
using System.Threading.Tasks;
using BDLabalytics.Helpers;
using BDLabalytics.Models;
using BDLabalytics.Services;
using Xamarin.Forms;

namespace BDLabalytics.ViewModel
{
    public class UserViewModel : BaseViewModel
    {
        private INavigation navigation;
        Command  _saveCommand;


        public UserModel UserModel;

        public string UID
        {
            get { return UserSettings.UID; }

        }

        string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (_firstName != value)
                {
                    _firstName = value;

                    OnPropertyChanged("FirstName");

                }
            }
        }
        string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (_lastName != value)
                {
                    _lastName = value;

                    OnPropertyChanged("LastName");

                }
            }
        }

        string _email;
        public string Email
        {
            get { return UserSettings.UserName; }
            set
            {
                if (_email != value)
                {
                    _email = value;

                    OnPropertyChanged("Email");

                }
            }
        }

        string _phoneNo;
        public string PhoneNo
        {
            get { return _phoneNo; }
            set
            {
                if (_phoneNo != value)
                {
                    _phoneNo = value;

                    OnPropertyChanged("PhoneNo");

                }
            }
        }

        readonly FirebaseHelper firebaseHelper = new FirebaseHelper();

        public UserViewModel()
        {
        }


        public Command SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new Command(() => ExecuteSaveChange()));
            }
        }
        private async void  ExecuteSaveChange()
        {
            System.Diagnostics.Debug.WriteLine("SaveClicked");
            //navigation.PushModalAsync(new CollectionHistoryPage());

            var userModel = new UserModel()
            {
                UID = UID,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                PhoneNo = PhoneNo
            };

            await firebaseHelper.AddUser(userModel);
            //DependencyService.Get<IRepository>().UpdateUserData(UID, userModel);
        }

        public async Task <UserModel>  FetchUserDetail()
        {
            var user = await firebaseHelper.GetUser(UID);

           // var user = DependencyService.Get<IRepository>().GetUserData(UID);

            return user;

        }



    }
}
