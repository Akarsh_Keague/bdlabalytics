﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using BDLabalytics.Helpers;
using BDLabalytics.Services;
using BDLabalytics.Validation;
using BDLabalytics.Views;
using Xamarin.Forms;

namespace BDLabalytics.ViewModel
{
    public class LoginViewModel:BaseViewModel
    {
        public Command  _loginCommand;
        

        readonly IFirebaseAuthenticator firebaseAuthenticator;

        Action propChangedCallBack => (LoginCommand as Command).ChangeCanExecute;


        string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName != value)
                {
                    _userName = value;

                    OnPropertyChanged("UserName");

                }
            }
        }
        string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;

                    OnPropertyChanged("Password");

                }
            }
        }



        public LoginViewModel(IFirebaseAuthenticator firebaseAuthenticator)
        {
            this.firebaseAuthenticator = firebaseAuthenticator;

            //UserName = new ValidatableObject<string>(propChangedCallBack, new EmailValidator()) { Value = "akarsh.keague@gmail.com" };
            //Password = new ValidatableObject<string>(propChangedCallBack, new PasswordValidator()) { Value = "Ad@7087879844" };
        }

        
        public Command LoginCommand
        {
            get
            {
                return _loginCommand ?? (_loginCommand = new Command(() => Login()));
            }
        }


        async Task Login()
        {
            IsBusy = true;
            propChangedCallBack();
            (Application.Current as App).AuthToken = await firebaseAuthenticator.LoginWithEmailPassword(UserName,Password);
            if ((Application.Current as App).AuthToken != null)
            {
                UserSettings.UserName = UserName;
                UserSettings.UID = (Application.Current as App).AuthToken;
                Application.Current.MainPage = new NavigationPage(new DashBoardPage());
            }

            IsBusy = false;
            propChangedCallBack();

        }
    }
}
