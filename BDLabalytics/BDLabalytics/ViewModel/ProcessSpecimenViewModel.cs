﻿using System;
using BDLabalytics.Views;
using Xamarin.Forms;

namespace BDLabalytics.ViewModel
{
    public class ProcessSpecimenViewModel : BaseViewModel
    {
        private INavigation navigation;

        Command _processSpecimenNextPageCommand, _processSpecimenCancelCommand,
            _captureResultCommand, _reCaptureResultCommand, _submitResultCommand,
            _navigateToMainMenuCommand;
        public ProcessSpecimenViewModel(INavigation navigation)
        {
            this.navigation = navigation;
        }
        public Command ProcessSpecimenNextPageCommand
        {
            get
            {
                return _processSpecimenNextPageCommand ?? (_processSpecimenNextPageCommand = new Command(() => ProcessSpecimenNextPage()));
            }
        }
        public Command ProcessSpecimenCancelCommand
        {
            get
            {
                return _processSpecimenCancelCommand ?? (_processSpecimenCancelCommand = new Command(() => ProcessSpecimenCancel()));
            }
        }
        public Command CaptureResultCommand
        {
            get
            {
                return _captureResultCommand ?? (_captureResultCommand = new Command(() => CaptureResult()));
            }
        }
        public Command ReCaptureResultCommand
        {
            get
            {
                return _reCaptureResultCommand ?? (_reCaptureResultCommand = new Command(() => ReCaptureResult()));
            }
        }
        public Command SubmitResultCommand
        {
            get
            {
                return _submitResultCommand ?? (_submitResultCommand = new Command(() => SubmitResult()));
            }
        }
        public Command NavigateToMainMenuCommand
        {
            get
            {
                return _navigateToMainMenuCommand ?? (_navigateToMainMenuCommand = new Command(() => NavigateToMainMenu()));
            }
        }
        bool isresultCaptured = true;
        public bool isResultCaptured
        {
            get { return isresultCaptured; }
            set
            {
                if (isresultCaptured != value)
                {
                    isresultCaptured = value;
                    AvilableToCaptureResult = !isresultCaptured;
                    OnPropertyChanged("isResultCaptured");
                }
            }
        }
        bool avilableToCaptureResult = false;
        public bool AvilableToCaptureResult
        {
            get { return avilableToCaptureResult; }
            set
            {
                if (avilableToCaptureResult != value)
                {
                    avilableToCaptureResult = value;

                    OnPropertyChanged("AvilableToCaptureResult");
                }
            }
        }
        private void ProcessSpecimenNextPage()
        {
            navigation.PushModalAsync(new ProcessSpecimenResult());
        }
        private void ProcessSpecimenCancel()
        {
            navigation.PopModalAsync();
        }
        private void CaptureResult()
        {
            isResultCaptured = true;
        }
        private void ReCaptureResult()
        {
            isResultCaptured = false;
        }
        private void SubmitResult()
        {
            navigation.PushModalAsync(new ProcessSpecimenCollectionComplete());
        }
        private void NavigateToMainMenu()
        {
            Application.Current.MainPage = new NavigationPage(new DashBoardPage());
        }
    }
}
