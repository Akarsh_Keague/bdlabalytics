﻿using System;
namespace BDLabalytics.ViewModel
{
    public class CollectionHistoryViewModel : BaseViewModel
    {

        string _firstName;
        public string FirstName
        {
            get { return _firstName; }
        }
        string _lastName;
        public string LastName
        {
            get { return _lastName; }
        }

        string _dob;
        public string DOB
        {
            get { return _dob; }
        }


        string _status;
        public string Status
        {
            get { return _status; }
        }

        public CollectionHistoryViewModel()
        {
        }
    }
}
