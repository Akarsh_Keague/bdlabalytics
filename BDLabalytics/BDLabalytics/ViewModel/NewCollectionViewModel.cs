﻿using System;
using BDLabalytics.Views;
using Xamarin.Forms;

namespace BDLabalytics.ViewModel
{
    public class NewCollectionViewModel : BaseViewModel
    {
        private INavigation navigation;
        bool isPatientExist = false;
        Command _cancelSearchPatientCommand, _searchPatientCommand,
                _nextFromCreatePatientCommand, _nextFromSignaturePageCommand,
            _submitTakeSampleCommand, _processSpecimenCommand, _navigateToMainMenuCommand,
            _startSampleCollectionCommand;
        public NewCollectionViewModel(INavigation navigation)
        {
            this.navigation = navigation;
        }
        public Command CancelSearchPatientCommand
        {
            get
            {
                return _cancelSearchPatientCommand ?? (_cancelSearchPatientCommand = new Command(() => CancelSearchPatient()));
            }
        }
        public Command SearchPatientCommand
        {
            get
            {
                return _searchPatientCommand ?? (_searchPatientCommand = new Command(() => SearchPatient()));
            }
        }
        public Command NextFromCreatePatientCommand
        {
            get
            {
                return _nextFromCreatePatientCommand ?? (_nextFromCreatePatientCommand = new Command(() => NextFromCreatePatient()));
            }
        }
        public Command NextFromSignaturePageCommand
        {
            get
            {
                return _nextFromSignaturePageCommand ?? (_nextFromSignaturePageCommand = new Command(() => NextFromSignaturePage()));
            }
        }
        public Command SubmitTakeSampleCommand
        {
            get
            {
                return _submitTakeSampleCommand ?? (_submitTakeSampleCommand = new Command(() => SubmitTakeSample()));
            }
        }
        public Command ProcessSpecimenCommand
        {
            get
            {
                return _processSpecimenCommand ?? (_processSpecimenCommand = new Command(() => ProcessSpecimen()));
            }
        }
        public Command NavigateToMainMenuCommand
        {
            get
            {
                return _navigateToMainMenuCommand ?? (_navigateToMainMenuCommand = new Command(() => NavigateToMainMenu()));
            }
        }
        public Command StartSampleCollectionCommand
        {
            get
            {
                return _startSampleCollectionCommand ?? (_startSampleCollectionCommand = new Command(() => StartSampleCollection()));
            }
        }
        private void CancelSearchPatient()
        {
            navigation.PopModalAsync();
        }

        private void SearchPatient()
        {
            if (isPatientExist)
                navigation.PushModalAsync(new NewCollectionPatientFound());
            else
                navigation.PushModalAsync(new NewCollectionCreatePatientPage());
        }
        private void NextFromCreatePatient()
        {
            navigation.PushModalAsync(new NewCollectionSignaturePage());
        }
        private void NextFromSignaturePage()
        {
            navigation.PushModalAsync(new NewCollectionTakeSample());
        }
        private void SubmitTakeSample()
        {
            navigation.PushModalAsync(new NewCollectionCompleteCollectionPage());
        }
        private void ProcessSpecimen()
        {
            navigation.PushModalAsync(new ProcessSpecimenPage());
        }
        private void NavigateToMainMenu()
        {
            Application.Current.MainPage = new NavigationPage(new DashBoardPage());
        }
        private void StartSampleCollection()
        {
            navigation.PushModalAsync(new NewCollectionTakeSample());
        }
    }
}
