﻿using System;
using BDLabalytics.Helpers;
using BDLabalytics.Views;
using Xamarin.Forms;

namespace BDLabalytics.ViewModel
{
    public class DashBoardViewModel : BaseViewModel
    {
        private INavigation navigation;
        Command _newCollectionCommand, _processSpecimenCommand, _collectionHistoryCommand, _userCommand;

        public DashBoardViewModel(INavigation navigation)
        {
            this.navigation = navigation;
        }


        public string UserName
        {
            get { return  UserSettings.UserName; }
            
        }

        public Command NewCollectionCommand
        {
            get
            {
                return _newCollectionCommand ?? (_newCollectionCommand = new Command(() => NavigateToNewCollection()));
            }
        }



        public Command ProcessSpecimenCommand
        {
            get
            {
                return _processSpecimenCommand ?? (_processSpecimenCommand = new Command(() => NavigateToProcessSpecimen()));
            }
        }


        public Command CollectionHistoryCommand
        {
            get
            {
                return _collectionHistoryCommand ?? (_collectionHistoryCommand = new Command(() => NavigateToCollectionHistory()));
            }
        }

        public Command UserCommand
        {
            get
            {
                return _userCommand ?? (_userCommand = new Command(() => NavigateToUser()));
            }
        }

        private void NavigateToNewCollection()
        {
             navigation.PushModalAsync(new NewCollectionPage());
        }
        private void NavigateToProcessSpecimen()
        {
            navigation.PushModalAsync(new ProcessSpecimenPage());
        }

        private void NavigateToCollectionHistory()
        {
            navigation.PushModalAsync(new CollectionHistoryPage());
        }

        private void NavigateToUser()
        {
            navigation.PushModalAsync(new UserPage());
        }
    }
}
