using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Truelove.ttf", Alias = "DummyFont")]
[assembly: ExportFont("Futura.ttf", Alias = "Futura")]
[assembly: ExportFont("FuturaBold.ttf", Alias = "FuturaB")]
[assembly: ExportFont("FuturaHeavy.ttf", Alias = "FuturaH")]
[assembly: ExportFont("FuturaMedium.ttf", Alias = "FuturaM")]