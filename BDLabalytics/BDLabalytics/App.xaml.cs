﻿using System;
using Autofac;
using BDLabalytics.Helpers;
using BDLabalytics.Services;
using BDLabalytics.ViewModel;
using BDLabalytics.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace BDLabalytics
{
    public partial class App : Application
    {
        public string AuthToken { get; set; }
        public IContainer Container { get; }

        public App(Module module)
        {
            InitializeComponent();
            Container = BuildContainer(module);
            MainPage =new LoginPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        IContainer BuildContainer(Module module)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<LoginViewModel>().AsSelf();
         
            builder.RegisterModule(module);
            return builder.Build();
        }
        public static void RegisterViewModels(INavigation navigation)
        {
            ServiceContainer.Register(() => new DashBoardViewModel(navigation));
            ServiceContainer.Register(() => new NewCollectionViewModel(navigation));
            ServiceContainer.Register(() => new ProcessSpecimenViewModel(navigation));
            ServiceContainer.Register(() => new UserViewModel());
        }
    }
}
