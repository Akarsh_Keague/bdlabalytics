﻿using System;
namespace BDLabalytics.Dependency
{
    public interface INetworkConnection
    {
        bool IsConnected { get; }

        void CheckNetworkConnection();
    }
}
