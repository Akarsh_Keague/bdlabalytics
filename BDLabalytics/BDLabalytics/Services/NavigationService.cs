﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BDLabalytics.Services
{
    public class NavigationService
    {
        public Task PushAsync(Page page) => GetCurrentNavigation().PushAsync(page);
        INavigation GetCurrentNavigation() => (Application.Current as App).MainPage.Navigation;
    }
}
