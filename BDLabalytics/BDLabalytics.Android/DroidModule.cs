﻿using System;
using Autofac;
using BDLabalytics.Droid.Services;
using BDLabalytics.Services;

namespace BDLabalytics.Droid
{
	public class DroidModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<FirebaseAuthenticator>().As<IFirebaseAuthenticator>().SingleInstance();
		}
	}
}
