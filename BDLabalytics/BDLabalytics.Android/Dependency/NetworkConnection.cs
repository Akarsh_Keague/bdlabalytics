﻿using System;
using Android.Net;
using BDLabalytics.Dependency;
using BDLabalytics.Droid.Dependency;
using Android.Content;
using Xamarin.Forms;

[assembly: Dependency(typeof(NetworkConnection))]
namespace BDLabalytics.Droid.Dependency
{
    public class NetworkConnection : INetworkConnection
    {
        public bool IsConnected { get; set; }
        public void CheckNetworkConnection()
        {
            var connectivityManager = (ConnectivityManager)global::Android.App.Application.Context.GetSystemService(Context.ConnectivityService);
            var activeNetworkInfo = connectivityManager.ActiveNetworkInfo;
            if (activeNetworkInfo != null && activeNetworkInfo.IsConnectedOrConnecting)
            {
                IsConnected = true;
            }
            else
            {
                IsConnected = false;
            }
        }
    }
}
